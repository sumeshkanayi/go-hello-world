FROM ubuntu
RUN apt update
RUN apt install -y curl
MAINTAINER sumesh
COPY go-hello-world /go-hello-world
ENV PG_USER postgres
ENV PG_PASSWD blah
ENV PG_HOST localhost
EXPOSE 9087/tcp
VOLUME [ "/data" ]
ADD input.txt /data-input.txt
ADD app-entrypoint.sh /app-entrypoint.sh
RUN chmod +x /app-entrypoint.sh
ENTRYPOINT [ "/app-entrypoint.sh" ]


