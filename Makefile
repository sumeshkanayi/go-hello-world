SHELL := /bin/bash
COMMIT_ID=$(shell git rev-parse --short HEAD)
default:	build db test push
build:
	env GOOS=linux GOARCH=arm64 go build
	docker build -t go-hello-world:${COMMIT_ID} .
test:
	docker volume create hello-world-data
	docker rm -f gh-3.0-1-g944519b
	docker run -d --name gh-3.0-1-g944519b -v hello-world-data:/data -p 9088:9087 -e PG_USER=hello -e PG_DB=hello-db -e PG_PASSWD=Sia123 -e PG_HOST="192.168.1.70" go-hello-world:${COMMIT_ID}
	curl 192.168.1.70:9088/write?msg="blah"
push:
		docker login -u skanayi -p $UR_PASSWORD
		docker tag go-hello-world:${COMMIT_ID} skanayi/hello-world:${COMMIT_ID}
		docker push skanayi/hello-world:${COMMIT_ID}
db:
	docker rm -f hello-postgres
	docker volume rm -f hello-pg
	docker build -f Dockerfile.postgres -t skanayi/hello-postgres:${COMMIT_ID} .
	sleep 10
	docker volume create hello-pg
	docker run -it -d --name hello-postgres -e POSTGRES_DB=hello-db -e POSTGRES_PASSWORD=Sia123 -e POSTGRES_USER=hello -v hello-pg:/var/lib/postgresql/data -p 5432:5432 skanayi/hello-postgres:${COMMIT_ID}


