package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func hello(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "world\n")
}
func writeToDb(w http.ResponseWriter, req *http.Request) {

	msg := req.URL.Query().Get("msg")
	pgUser := os.Getenv("PG_USER")
	pgPasswd := os.Getenv("PG_PASSWD")
	pgHost := os.Getenv("PG_HOST")

	//psqlconn := fmt.Sprintf("postgres://hello:Sia123@192.168.1.70/hello-db?sslmode=disable",pgUser,pgPasswd,pgHost)

	psqlconn := fmt.Sprintf("postgres://%s:%s@%s/hello-db?sslmode=disable", pgUser, pgPasswd, pgHost)

	fmt.Println("psql connection string is", psqlconn)
	db, err := sqlx.Connect("postgres", psqlconn)

	tx := db.MustBegin()
	if err != nil {
		fmt.Println("error occured", err)
		os.Exit(1)
	}

	defer db.Close()

	// insert
	// hardcoded

	tx.MustExec("INSERT INTO public.hello (message) VALUES ($1)", msg)
	if err != nil {
		fmt.Println("error occured", err)
		os.Exit(1)
	}

	err = tx.Commit()

	if err != nil {
		fmt.Fprintf(w, "error 500 \n")
	}
	fmt.Fprintf(w, "aLL GOOD")

}
func main() {

	_, err := os.Stat("/data-input.txt")

	if err != nil {
		fmt.Println("you didnt give me the input file")
		os.Exit(1)
	}

	err = os.WriteFile("/data/output.txt", []byte("hello world"), 0777)

	if err != nil {

		fmt.Println("error writing output", err)
	}

	http.HandleFunc("/hello", hello)
	http.HandleFunc("/write", writeToDb)
	http.ListenAndServe(":9087", nil)
}
